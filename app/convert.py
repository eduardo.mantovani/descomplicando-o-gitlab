import glob
from pathlib import Path
import datetime
import time
import subprocess
import create_list
import mysql.connector
import hashlib

db = mysql.connector.connect(
  host="192.168.86.35",
  user="root",
  password="GiropopsXXX",
  database="converter"
  )
  

def get_from_db(args):
    select_video_to_convert = "SELECT name FROM converter.list WHERE converted = 'False'"
    cursor = db.cursor(buffered=True)
    cursor.execute(select_video_to_convert)
    result = cursor.fetchall()
    for row in result:
        line_to_add = str(row[0])
        converter(line_to_add)
        update_video_to_converted = "UPDATE converter.list SET converted = '1' WHERE name = '%s'" % (line_to_add)
        update_cursor = db.cursor(buffered=True)
        update_cursor.execute(update_video_to_converted)
        db.commit()

def converter(line):
    name = line
    if name.lower().endswith(('.mp4', '.mov', '.avi')):
        print("-" * 88)
        print("%s - Starting the convertion of %s" % (datetime.datetime.now(), name))
        file_name = Path(name).stem
        path = Path(name).parent
        file_extension = Path(name).suffixes
        file_name_converted = "%s/%s-1080p%s" % (path, file_name, file_extension[0]) 
        FFMPEG_CONVERT_COMMAND = "ffmpeg -loglevel quiet -i %s -vf scale=1920:1080 -c:v libx264 -crf 20 -preset slow %s" % (name, file_name_converted)
        FFMPEG_TEST_ORIGINAL_COMMAND = "ffmpeg -v error -i %s -f null -" % (name)
        FFMPEG_TEST_CONVERTED_COMMAND = "ffmpeg -v error -i %s -f null -" % (file_name_converted)
        if Path(file_name_converted).is_file():
            print("\t%s - [PROBLEM] - The converted file already exist - %s" % (datetime.datetime.now(), file_name_converted))
        testing_original = subprocess.call(FFMPEG_TEST_ORIGINAL_COMMAND, shell=True)
        if testing_original == 0:
            print("\t%s - Original file is not corrupted! - %s" % (datetime.datetime.now(), name))
            print("\t%s - Starting to convert the file - %s" % (datetime.datetime.now(), name))
            converting = subprocess.call(FFMPEG_CONVERT_COMMAND, shell=True)
            if converting == 0:
                testing = subprocess.call(FFMPEG_TEST_CONVERTED_COMMAND, shell=True)
                if testing == 0:
                    insert_video_name_converted = "UPDATE converter.list SET name_converted = '%s' WHERE name = '%s'" % (file_name_converted, name)
                    update_cursor = db.cursor(buffered=True)
                    update_cursor.execute(insert_video_name_converted)
                    db.commit()
                    file_to_test = open(file_name_converted, "rb")
                    data = file_to_test.read()
                    md5_returned_converted = hashlib.md5(data).hexdigest()
                    insert_md5_cursor = db.cursor(buffered=True)
                    sql = "UPDATE converter.list SET md5_file_converted = '%s' WHERE name = '%s'" % (md5_returned_converted, name)
                    insert_md5_cursor.execute(sql)
                    db.commit()
                    print("\t%s - [CONVERTED] - %s" % (datetime.datetime.now(), file_name_converted))
            else:
                print("\t%s - [PROBLEM] - The converted file is corrupted - %s" % (datetime.datetime.now(), file_name_converted))
                insert_video_name_converted = "UPDATE converter.list SET failed = '1' WHERE name = '%s'" % (name)
                update_cursor = db.cursor(buffered=True)
                update_cursor.execute(insert_video_name_converted)
                db.commit()       
        else:
            print("\t%s - [PROBLEM] - The original file is corrupted - %s" % (datetime.datetime.now(), name))
            insert_video_name_converted = "UPDATE converter.list SET failed = '1' WHERE name = '%s'" % (name)
            update_cursor = db.cursor(buffered=True)
            update_cursor.execute(insert_video_name_converted)
            db.commit()  
        print("-" * 88)
        print("")
    return